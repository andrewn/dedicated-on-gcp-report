"use strict";

const urlStreamer = require("gitlab-issue-report-kit").urlStreamer;

class FindDiscussionsForMergeRequests {
  async query (mrLinks) {
    const results = new Map();

    for (const mrLink of mrLinks) {
      const allForItem = [];
      for await (const discussion of urlStreamer(`${mrLink}/discussions`)) {
        allForItem.push(discussion);
      }

      results.set(mrLink, allForItem);
    }

    return results;
  }
}

module.exports = FindDiscussionsForMergeRequests;
