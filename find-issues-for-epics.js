"use strict";

const urlStreamer = require("gitlab-issue-report-kit").urlStreamer;

class FindIssuesForEpics {
  async query (groupId, epicIds) {
    const results = [];

    for (const epicId of epicIds) {
      for await (const issue of urlStreamer(`https://gitlab.com/api/v4/groups/${groupId}/issues?epic_id=${epicId}`)) {
        results.push(issue);
      }
    }

    return results;
  }
}

module.exports = FindIssuesForEpics;
