"use strict";

const simpleStats = require("simple-statistics");
const distanceInWords = require("date-fns/distance_in_words");

const quantiles = [5, 25, 50, 75, 95];

function projectFromWebUrl (webUrl) {
  return webUrl.replace(/\/-\/merge_requests\/\d+$/, "");
}

class ReviewDurationStats {
  constructor () {
    this.allSamples = [];
    this.times = new Map();
  }

  addDurationForMergeRequest (webUrl, duration) {
    const project = projectFromWebUrl(webUrl);
    if (!this.times.has(project)) {
      this.times.set(project, []);
    }

    this.allSamples.push(duration);
    this.times.get(project).push(duration);
  }

  summary () {
    const totalValues = quantiles.map(q => {
      const v = simpleStats.quantile(this.allSamples, q / 100);
      const humanDistance = secondsToDistance(v);
      return humanDistance;
    });

    const perProject = Array.from(this.times.entries()).map(([project, projectSamples]) => {
      const values = quantiles.map(q => {
        const v = simpleStats.quantile(projectSamples, q / 100);
        const humanDistance = secondsToDistance(v);
        return humanDistance;
      });

      return { project, size: projectSamples.length, values };
    });

    return {
      quantiles,
      size: this.allSamples.length,
      totalValues,
      perProject
    };
  }
}

function secondsToDistance (s) {
  const n = Date.now();

  return distanceInWords(
    new Date(n - s),
    new Date(n)
  );
}

module.exports = ReviewDurationStats;
