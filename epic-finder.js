"use strict";

const urlStreamer = require("gitlab-issue-report-kit").urlStreamer;

class EpicFinder {
  async query (groupId, epicIid) {
    const epicMap = new Map();
    const allEpics = [];
    const allNestedEpics = new Map();
    let topEpic = null;

    // Epic API has no ability to find child epics :facepalm:
    for await (const epic of urlStreamer(`https://gitlab.com/api/v4/groups/${groupId}/epics`)) {
      allEpics.push(epic);
      if (String(epic.iid) === String(epicIid)) {
        topEpic = epic;
      }

      const parentIid = String(epic.parent_iid);
      if (!parentIid) {
        continue;
      }

      if (epicMap.has(parentIid)) {
        epicMap.get(parentIid).push(epic);
      } else {
        epicMap.set(parentIid, [epic]);
      }
    }


    const findNestedEpics = (epicIid) => {
      epicIid = String(epicIid);
      const childEpics = epicMap.get(epicIid);

      if (!childEpics) return;

      for (const childEpic of childEpics) {
        const childIid = String(childEpic.iid);
        if (allNestedEpics.has(childIid)) {
          // Already added
          continue;
        }

        allNestedEpics.set(childIid, childEpic);
        findNestedEpics(childIid);
      }
    };

    if (topEpic) {
      allNestedEpics.set(topEpic.iid, topEpic);
      findNestedEpics(topEpic.iid);

      return Array.from(allNestedEpics.values());
    }

    throw new Error("top level epic not found");
  }
}

module.exports = EpicFinder;
