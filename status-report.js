"use strict";

const purgeCache = require("gitlab-issue-report-kit").purgeCache;
const format = require("date-fns/format");
const nunjucks = require("nunjucks");
const distanceInWords = require("date-fns/distance_in_words");
const Report = require("gitlab-issue-report-kit").Report;
const config = require("./config.json");
const EpicFinder = require("./epic-finder");
const FindIssuesForEpics = require("./find-issues-for-epics");
const FindRelatedMergeRequestsForIssues = require("./find-related-merge-requests-for-issues");
const FindDiscussionsForMergeRequests = require("./find-discussions-for-merge-requests");
const fs = require("node:fs/promises");
const { DateTime } = require("luxon");

const weightMap = new Map([
  [1, "XXS"],
  [2, "XS"],
  [3, "S"],
  [5, "M"],
  [8, "L"],
  [13, "XL"],
  [21, "XXL"]
]);

function sizeLabel(weight) {
  const size = weightMap.get(weight) || "?";

  return `${size} (${weight} points)`;
}

class StatusReportGenerator {
  constructor (groupId, epicIid) {
    this.groupId = groupId;
    this.epicIid = epicIid;
    this.since = this.getStartTime();
    console.error("# Starting report at ", this.since);
  }

  getStartTime () {
    return DateTime.local()
      .setZone("UTC")
      .startOf("week").plus({ days: -3 })
      .toJSDate();
  }

  async collectData () {
    // Fetch all epics and subepics
    const allEpics = await new EpicFinder().query(this.groupId, this.epicIid);

    const epicIds = allEpics.map(f => f.id);

    // Find all issues for these epics that have been updated since start time
    const issues = await new FindIssuesForEpics().query(this.groupId, epicIds, this.since);

    const issuesClosed = issues.filter(f => f.closed_at && new Date(f.closed_at) >= this.since);
    // Filter out any issues that were closed before the since time
    const filteredIssues = issues.filter(f => !f.closed_at || new Date(f.closed_at) >= this.since);

    const issueLinks = filteredIssues.map(f => f._links.self);

    const relatedMergeRequests = await new FindRelatedMergeRequestsForIssues().query(issueLinks);
    const allMergeRequests = Array.from(relatedMergeRequests.values()).flat();

    const uniqMap = new Map();
    allMergeRequests.forEach(f => uniqMap.set(f.web_url, f));

    const uniqMergeRequests = Array.from(uniqMap.values());

    const mrLinks = uniqMergeRequests.map(f => `https://gitlab.com/api/v4/projects/${f.project_id}/merge_requests/${f.iid}`);
    const allDiscussions = await new FindDiscussionsForMergeRequests().query(mrLinks);

    return {
      allEpics,
      issues,
      issuesClosed,
      allMergeRequests: uniqMergeRequests,
      allDiscussions: Object.fromEntries(allDiscussions),
    };
  }

  async getStatusReport () {
    let inputData;
    try {
      inputData = JSON.parse(await fs.readFile("queries.json"));
    } catch (e) {
      inputData = await this.collectData();
      await fs.writeFile("queries.json", JSON.stringify(inputData));
    }

    const {
      allEpics,
      issues,
      issuesClosed,
      allMergeRequests,
      allDiscussions
    } = inputData;

    this.enhance(allMergeRequests, allDiscussions);

    const mergedMRs = allMergeRequests.filter(f => f.merged_at && new Date(f.merged_at) >= this.since);
    const openMRSInReview = allMergeRequests.filter(f => !f.merged_at && !f.closed_at && f.reviewers.length > 0 && !f.draft);

    mergedMRs.sort((a, b) => new Date(a.merged_at) - new Date(b.merged_at));
    openMRSInReview.sort((a, b) => new Date(a.created_at) - new Date(b.created_at));

    let totalTime = 0;
    let totalSinceReview = 0;
    allMergeRequests.forEach(mr => {
      if (mr.totalTime && mr.totalSinceReview) {
        totalTime += mr.totalTime;
        totalSinceReview += mr.totalSinceReview;
      }
    });

    const totalStoryPoints = issuesClosed.reduce((memo, issue) => memo + (issue.weight || 0), 0);
    issuesClosed.sort((i1, i2) => (i2.weight || 0) - (i1.weight || 0));

    const env = nunjucks.configure();
    env.addFilter("sizeLabel", sizeLabel, false);

    const body = await env.render("tps.tmpl.md", {
      issuesClosed,
      mergedMRs,
      openMRSInReview,
      totalTime,
      totalSinceReview,
      totalStoryPoints
    });

    return new Report(
      this.since, config.project, config.label, true, config.titlePrefix, body
    );
  }

  isReviewRequestedNote (note) {
    return note.system && note.body?.match(/^requested review/);
  }

  enhance (mergeRequests, allDiscussions) {
    for (const mr of mergeRequests) {
      const link = `https://gitlab.com/api/v4/projects/${mr.project_id}/merge_requests/${mr.iid}`;
      const discussions = allDiscussions[link];

      let dateOfFirstReviewRequest = null;
      for (const d of discussions) {
        const note = d.notes[0];
        if (this.isReviewRequestedNote(note)) {
          dateOfFirstReviewRequest = new Date(note.created_at);
          break;
        }
      }

      let endTime;
      const startTime = new Date(mr.created_at);
      if (mr.state === "opened") {
        endTime = new Date();
      } else if (mr.state === "merged") {
        endTime = new Date(mr.merged_at);
      }

      const total = endTime - startTime;
      let totalSinceReview = null;
      if (dateOfFirstReviewRequest) {
        totalSinceReview = endTime - dateOfFirstReviewRequest;
      }

      mr.totalTime = total;
      mr.totalTimeFormatted = distanceInWords(
        startTime,
        endTime
      );

      mr.totalSinceReview = totalSinceReview;
      mr.dateOfFirstReviewRequest = dateOfFirstReviewRequest;
      mr.reviewTimeFormatted = distanceInWords(
        dateOfFirstReviewRequest,
        endTime
      );

      mr.dateOfFirstReviewRequestFormatted = mr.dateOfFirstReviewRequest ? format(mr.dateOfFirstReviewRequest, "YYYY-MM-DD") : "";

      const reviewPercent = mr.totalSinceReview / mr.totalTime;
      mr.reviewPercent = reviewPercent >= 0 && reviewPercent <= 1 ? Math.round(reviewPercent * 100) : ""
    }
  }
}

async function statusReport (groupId, epicIid) {
  try {
    const statusReportGenerator = new StatusReportGenerator(groupId, epicIid);

    return await statusReportGenerator.getStatusReport();
  } finally {
    try {
      await purgeCache();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error("Cache purge failed: " + e);
    }
  }
}

module.exports = statusReport;
