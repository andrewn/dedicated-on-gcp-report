"use strict";

const urlStreamer = require("gitlab-issue-report-kit").urlStreamer;

class FindRelatedMergeRequestsForIssues {
  async query (issueLinks) {
    const results = new Map();

    for (const issueLink of issueLinks) {
      const allForIssue = [];
      for await (const relatedMR of urlStreamer(`${issueLink}/related_merge_requests`)) {
        allForIssue.push(relatedMR);
      }

      results.set(issueLink, allForIssue);
    }

    return results;
  }
}

module.exports = FindRelatedMergeRequestsForIssues;
