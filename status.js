#!/usr/bin/env node
/* eslint-disable no-console */

"use strict";

const statusReport = require("./status-report");
const cleanOldReports = require("gitlab-issue-report-kit").cleanOldReports;
const persistStatusReport = require("gitlab-issue-report-kit").persistStatusReport;

async function main () {
  const report = await statusReport(12125409, "157"); // https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/157
  if (!report) return;

  const persisted = await persistStatusReport(report);
  if (!persisted) return;

  console.log(`# Report persisted to ${persisted.web_url}`);
  await cleanOldReports(persisted.iid, report);
}

main()
  .catch(err => {
    console.error(err);
    process.exit(1);
  })
  .then(() => {
    process.exit(0);
  });
