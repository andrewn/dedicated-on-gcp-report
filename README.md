# dedicated-on-gcp-report

This project generates an up-to-date report on all activity on the [Dedicated on GCP Project](https://gitlab.com/groups/gitlab-com/gl-infra/gitlab-dedicated/-/epics/157), so that the weekly status report can be done with minimal effort.

View the report at https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/?label_name%5B%5D=Dedicated%20on%20GCP%20Project%20Report.

## Hacking on `dedicated-on-gcp-report`

This project uses [`asdf`](https://asdf-vm.com/guide/getting-started.html) for Tool Version management. This guide assumes you are using `asdf`.

```console
$ # install asdf node plugin
$ asdf plugin add nodejs
$ # install tool dependencies
$ asdf install # or rtx install
$ # install node modules
$ npm install
$ # export a GitLab OAuth2 access token
$ eval $(pmv gitlab env)
$ # Run the report
$ ./status.js
```

For convenience, this project uses [`pre-commit`](https://pre-commit.com/). You can setup hooks with `pre-commit install`.
