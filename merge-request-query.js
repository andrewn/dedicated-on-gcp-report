"use strict";

const urlStreamer = require("gitlab-issue-report-kit").urlStreamer;
const httpClient = require("gitlab-issue-report-kit").httpClient;
const subMonths = require("date-fns/sub_months");
const ReviewDurationStats = require("./review-duration-stats");
const collectResults = require("gitlab-issue-report-kit/lib/collect-results");

function isReviewRequestedNote (note) {
  return note.system && note.body?.match(/^requested review/);
}

function isApprovalNote (note) {
  return note.system && note.body === "approved this merge request";
}

class MergeRequestQuery {
  async query (since) {
    const results = [];
    const now = Date.now();

    const openReviewStats = new ReviewDurationStats();
    const combinedReviewStats = new ReviewDurationStats();

    // Iterate over open MRs
    for await (const mr of urlStreamer("https://gitlab.com/api/v4/groups/gitlab-com%2fgl-infra%2fgitlab-dedicated/merge_requests?scope=all&state=opened&wip=no")) {
      // group_12125409_bot -> renovate. Ignore for now.
      if (mr.reviewers == null || !mr.reviewers.length || mr.author.username === "group_12125409_bot") {
        continue;
      }

      mr.discussions = [];
      mr.firstReviewRequestedDate = null;
      mr.lastReviewRequestedDate = null;
      mr.participants = new Map();

      const discussions = await collectResults(`https://gitlab.com/api/v4/projects/${mr.project_id}/merge_requests/${mr.iid}/discussions`);
      const allNotes = discussions.flatMap(d => d.notes);

      const firstReviewRequested = allNotes.find(isReviewRequestedNote);
      allNotes.forEach(note => {
        if (!note.system) {
          if (note.author.username !== mr.author.username) {
            mr.participants.set(note.author.username, note.author);
          }
        }
      });

      if (firstReviewRequested) {
        if (!mr.firstReviewRequest) {
          const firstDate = new Date(Date.parse(firstReviewRequested.created_at));
          mr.firstReviewRequestedDate = firstDate;

          openReviewStats.addDurationForMergeRequest(mr.web_url, now - firstDate);
          combinedReviewStats.addDurationForMergeRequest(mr.web_url, now - firstDate);
        }
      }

      const [approvals] = await httpClient(`https://gitlab.com/api/v4/projects/${mr.project_id}/merge_requests/${mr.iid}/approvals`);
      mr.approvals = approvals;

      results.push(mr);
    }

    const createdSince = subMonths(since, 1);

    // Iterate over MRs closed in the past month
    for await (const mr of urlStreamer(`https://gitlab.com/api/v4/groups/gitlab-com%2fgl-infra%2fgitlab-dedicated/merge_requests?scope=all&state=merged&created_after=${createdSince.toISOString()}`)) {
      if (mr.reviewers == null || !mr.reviewers.length || mr.merged_by.username === "group_12125409_bot" || mr.author.username === "group_12125409_bot") {
        continue;
      }

      const discussions = await collectResults(`https://gitlab.com/api/v4/projects/${mr.project_id}/merge_requests/${mr.iid}/discussions`);
      const allNotes = discussions.flatMap(d => d.notes);

      const firstReviewRequested = allNotes.find(isReviewRequestedNote);
      const wasApproved = allNotes.some(isApprovalNote);

      if (firstReviewRequested && wasApproved) {
        // Record time from first review until merge...
        const mergedAt = new Date(Date.parse(mr.merged_at));
        const firstDate = new Date(Date.parse(firstReviewRequested.created_at));

        combinedReviewStats.addDurationForMergeRequest(mr.web_url, mergedAt - firstDate);
      }
    }

    return {
      openMRs: results,
      combinedReviewStats,
      openReviewStats
    };
  }
}

module.exports = MergeRequestQuery;
